﻿/*
Napisać program, który wczyta dowolnie wielki zestaw danych ze standardowego wejścia, obliczy wartość średnią i odchylenie standardowe oraz wyświetli obliczone wartości na standardowe wyjście.

Etapy rozwiązania (po 1 pkt.):
    1. Funkcja double *wczytaj_dane (FILE *strumien) wczytująca dane z podanego strumienia - zarys.
    2. Dynamiczna (re)alokacja famięci w ww. funkcji (2 pkt.).
    3. Funkcja void oblicz_statystyke (const double *dane, size_t rozmiar, double *srednia, double *sigma) obliczająca wartość średnią oraz sigmę dla próbki danych o wskazanym rozmiarze - zarys.
    4. Obliczenie estymatora wartości średniej w ww. funkcji.
    5. Obliczenie estymatora odchylenia standardowego w ww. funkcji.
    6. Poprawne wywołanie obu funkcji w finkcji main().
    7. Wyświetlenie obliczonych wartości na standardowe wyjścia.
*/

#include <stdio.h>
#include <stdlib.h>

double *wczytaj_dane(FILE *strumien) {
    float bufor;                                                                // Bufor do przechowywania wczytanej wartości
    int index = 0;                                                              // Index tablicy
    double *dane = NULL;                                                        // Inicjalizacja tablicy

    while (feof(strumien) == 0) {                                               // Jeżeli program nie napotkał końca pliku wykonuje pętle
        fscanf(strumien, "%f\n", &bufor);                                       //  Wczytuje float do bufora (liczby z pliku mają za dużą dokładność by przenieść je bezpośrednio do double)
        dane = (double*)realloc(dane, (index + 1) * sizeof(double));            //  Realokuję pamięć dla kolejnego double
        dane[index] = (double)bufor;                                            //  Zapisuję z buforu do tablicy
        index++;                                                                //  Zwiększam index
    }

    return dane;
}

void oblicz_statystyke(const double *dane, size_t rozmiar, double *srednia, double *sigma) {

}

int main() {
    double *dane;                                                               // Zmienna do przechowywania danych
    FILE *plik = fopen("dane.txt", "r");                                        // Otwórz plik z uprawnieniami tylko do odczytu

    dane = wczytaj_dane(plik);                                                  // Wczytaj dane do tablicy

    fclose(plik);                                                               // Zamknij strumień
    getchar();                                                                  // Poczekaj na dowolny przycisk z klawiatury
    return 0;                                                                   // Zwróć kod pozytywnego zamknięcia programu
}